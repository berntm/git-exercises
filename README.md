# Git exercises

Sources for the [**Git Workshop book**](https://oesa.pages.ufz.de/git-exercises) of the UFZ Department Ecological Modelling (OESA).

The slides for the theory parts of the workshop are available as PDF [here](https://git.ufz.de/oesa/git-exercises/-/jobs/artifacts/master/raw/slides/slides.pdf?job=slides).

## Content

[Introduction](src/introduction.md)

* [Exercise 1](src/exercise-1.md) - Linear workflow
* [Exercise 2](src/exercise-2.md) - Non-linear workflow
* [Exercise 3](src/exercise-3.md) - Sharing changes
* [Exercise 4](src/exercise-4.md) - Collaborate using GitLab
* [Exercise 5](src/exercise-5.md) - Ignoring files
* [Exercise 6](src/exercise-6.md) - Tags for reproducibility
* [Exercise 7](src/exercise-7.md) - Keep branches up-to-date

[CheatSheet Command Line](src/cheatsheet-cmd.md)  
[CheatSheet Git Commands](src/cheatsheet-git.md)

## Contributing

If you have any suggestions how to improve the book, please [create an issue](https://git.ufz.de/oesa/git-exercises/-/issues) or make a merge request with your suggested changes.
