# Exercise 4 - Collaborate using GitLab

In this exercise, you will learn how to use the most essential collaboration features of GitLab. You will create and manage issues and create, discuss and finalize a Merge Request.

[[_TOC_]]

## Create an issue

1. On your project's GitLab page, click `Issues` in the navigation at the left
2. Click `New issue` at the bottom of the page
3. Fill fields `Title` and `Description` with some content

Title

```plaintext
Better description in README.md
```

Description

```plaintext
Add a more informative description in file `README.md`

* [ ] What is the project about
* [ ] How to use the project
```

Check the Preview tab of the description field. Click `Submit issue` when you are fine with the content of the preview.

You should now see the page of the issue. There are several actions available at the right, and there is a text field at the bottom to start a discussion about the issue.

## Manage issues with boards

In the left navigation, under `Issues`, click `Boards`. A Kanban board with your issue in column `Open` will appear.

To create more lists, in addition to `Open` and `Closed`, click `Add list` (top right), and select `Create project label`. Enter `In Progress` as name, select a color and scroll down to click `Create`.

Drag the issue to column `In Progress`. This wil automatically label the issue with `In Progress`. In the left navigation, under `Issues`, click `List` to see that the issue is labelled now.

## Create a merge request

We will now fix the issue on a new branch and create a merge request for the fix.

Create and check out a new branch `better-description`

```plaintext
git checkout -b better-description
```

Edit file `README.md` by replacing its content with

```plaintext
# Git Workshop

Notes taken in the OESA Git workshop in early 2021.

Use the project by browsing the files linked below, and by trying the commands listed there.

Content

* [Linear workflow](linear-workflow.md)
* [Inspecting](inspecting.md)
* [Amending commits](amending.md)
```

Commit as usual

```plaintext
git status
git diff
git add README.md
git commit -m "enhanced description in README.md"
```

Push the branch to the GitLab project

```plaintext
git push --set-upstream origin better-description
```

<details><summary>Output</summary>

```plaintext
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 760 bytes | 760.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for better-description, visit:
remote:   https://git.ufz.de/mlange/git-workshop/-/merge_requests/new?merge_request%5Bsource_branch%5D=better-description
remote:
To https://git.ufz.de/mlange/git-workshop.git
 * [new branch]      better-description -> better-description
Branch 'better-description' set up to track remote branch 'better-description' from 'origin'.
```
</details>

Go back to your project's GitLab page and refresh it. At the very top, there is a notification that you just pushed branch `better-description`, with a button `Create merge request`. Click that button.

You will see a form that looks very similar to the issue form. Fill the description with some informative content.

```md
Enhanced description according issue #1.

Fixes #1.
```

Check the Preview, and click `Submit merge request`.

Scan through the merge request's page. Also check the tabs `Commits` and `Changes`, right below the heading.

## Discuss changes

In tab `Changes` you can comment on individual lines. In the right half (the new version), hover over line 5. Left of the line, a small speech bubble icon will appear. Click it!

Enter a comment in the appearing text field, e.g.

```plaintext
We could add that the commands are Git commands.
```

Press `Add comment now`. Go back to the `Overview` tab. Notice the `1 unresolved thread` next to the heading. Click the speech bubble with the arrow next to it to jump to the thread. Below your comment is a text box for others to reply.

## Resolve threads

We will resolve the discussion by changing file `README.md` again. Open it locally with a text editor and replace the content with

```
# Git Workshop

Notes taken in the OESA Git workshop in early 2021.

Use the project by browsing the files linked below, and by trying the git commands listed there.

Content

* [Linear workflow](linear-workflow.md)
* [Inspecting](inspecting.md)
* [Amending commits](amending.md)
```

Check diffs and commit as usual

```plaintext
git status
git diff
git add README.md
git commit -m "added note that the commands are git commands"
```

Push again. Now that the branch was pushed and the upstream repository was set with `--set-upstream origin`, we can use a shorter form

```plaintext
git push
```

## Preview a branch

You can also preview the rendered version of the branch. Go to the project's main page by clicking `git-workshop` on the very top left.

Below ths statistics, there is a dropdown with `main`. Click it and select `better-description`. The page now shows the version on your branch.

If you scroll down and check the content, you may notice that we have a typo there. Git should be capitalized!

## Amend last commit

Open file `README.md` locally and fix the typo.

Inspect and stage changes

```plaintext
git status
git diff
git add README.md
git status
```

As this is a tiny change belonging to the previous commit, we amend this time

```plaintext
git commit --amend
```

Close the popup message editor.

We can now push again

```plaintext
git push
```

<details><summary>Output</summary>

```plaintext
To https://git.ufz.de/mlange/git-workshop.git
 ! [rejected]        better-description -> better-description (non-fast-forward)
error: failed to push some refs to 'https://git.ufz.de/mlange/git-workshop.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```
</details>

Oops, red text! You should always read Git's messages carefully! It says that "Updates were rejected because the tip of your current branch is behind its remote counterpart". Let's check the status, maybe this helps us

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch better-description
Your branch and 'origin/better-description' have diverged,
and have 1 and 1 different commits each, respectively.
  (use "git pull" to merge the remote branch into yours)

nothing to commit, working tree clean
```
</details>

It tells us that "Your branch and 'origin/better-description' have diverged, and have 1 and 1 different commits each, respectively." We can better see what is meant here by viewing the graph

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* 8226777 (HEAD -> better-description) added note that the commands are git commands
| * 89a8e14 (origin/better-description) added note that the commands are git commands
|/
* 39d2543 enhanced description in README.md
* f39b285 (origin/main, main) Update README.md
*   fc527c5 Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

The error message as well as `git status` suggest that we should pull, but that is not what we want. Instead, we have to force-push here with option `--force-with-lease`

```plaintext
git push --force-with-lease
```

> There is also the shorter option `--force`, but it is strongly discouraged to use it as it can be destructive if a collaborator (or you) already committed on top of the amended commit.

Refresh the repository's page (still on branch `better-description`) to see that the fix is there.

## Resolve the merge request

Go back to the merge request's page and jump or scroll to the unresolved thread. Then, either

* Press button `Resolve thread`, or
* Write something into the text box directly beneath (!) your first comment, tick `Resolve thread`, and press `Add comment now`

Now, all threads are resolved. Scroll up to the green `Merge` button and press it to merge the branch into `main`.

## Update local repository

Fetch the changes from the remote with option `--prune` to prune the merged and automatically deleted remote branch

```plaintext
git fetch --prune
```

Check out branch `main` and pull it

```plaintext
git checkout main
git pull
```

Check the graph

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
*   a3d243a (HEAD -> main, origin/main) Merge branch 'better-description' into 'main'
|\
| * 8226777 (better-description) added note that the commands are git commands
| * 39d2543 enhanced description in README.md
|/
* f39b285 Update README.md
*   fc527c5 Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

Delete the local branch `better-description`, which is not required anymore

```plaintext
git branch --delete better-description
```

Check the graph again

```plaintext
git log --graph --oneline --all
```

----

Continue with [Exercise 5](exercise-5.md)
