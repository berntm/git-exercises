# Exercise 1 - Linear workflow

In this exercise, you will learn the most basic Git commands for a simple linear workflow. You will make commits and inspect the project's history.

[[_TOC_]]

## Test the Git installation

Open the console ("Eingabeaufforderung").

To test your git installation, run the following, which should print the installed Git version

```plaintext
git --version
```

<details><summary>Output</summary>

```plaintext
git version 2.30.0.windows.2
```
</details>

## Git help

Help overview

```plaintext
git --help
```

Help on a specific command

```plaintext
git <command> -h
git <command> --help
```

## Git settings

**User name and email**

Git needs a user name and email address to attribute your changes. We set them with

```plaintext
git config --global user.name "<Your Yame>"
git config --global user.email "<your.name>@ufz.de"
```

**Line endings**

Line endings are differnt in Unix compared to Windows. therefore, we need to set how they are handled

```plaintext
git config --global core.autocrlf true
```

**Default text editor**

Further, we set the default text editor to `notepad`, the Windows default editor

```plaintext
git config --global core.editor notepad
```

**Default branch**

To avoid the outdated, oppressive default branch name `master`, which is perceived as being related to slavery, we change it to `main`

```plaintext
git config --global init.defaultBranch main
```

All these setting are now global, you don't need to repeat them for every new project.

## Project setup

Create a directory for your project (`C:\Projects\git-workshop`)

```plaintext
C:\Users\username>cd C:\
C:\>mkdir Projects
C:\>cd Projects
C:\Projects>mkdir git-workshop
C:\Projects>cd git-workshop
C:\Projects\git-workshop>
```

From here on, we only use `> ` for `C:\Projects\git-workshop>`.

## Initialize a Git repository

Initialization of a repository is done with a single, simple command

```plaintext
git init
```

<details><summary>Output</summary>

```plaintext
Initialized empty Git repository in C:/Projects/git-workshop/.git/
```
</details>

We can inspect our project's directory content (with the option to show hidden files)

```plaintext
dir /A:H
```

<details><summary>Output</summary>

```plaintext
...
27/01/2021  13:17    <DIR>          .git
               0 File(s)              0 bytes
               1 Dir(s)  517,128,151,040 bytes free
```
</details>

Git created a directory `.git`, which is where the repository history is stored. You don't need to do anything with this directory, just for your information...

## Make a first commit

Create a file `README.md`[^1]

```plaintext
echo.> README.md
```

> You can also create the file using the Explorer, but depending on your settings you may end up with `README.md.txt`, which may be complicated to change.

Open the file with your text editor and fill it with

```plaintext
# Git Workshop

Content

* ...
* ...
* ...
```

Save your changes.

View the status of your project

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        README.md

nothing added to commit but untracked files present (use "git add" to track)
```
</details>

We need to "stage" our changes to be able to commit

```plaintext
git add README.md
```

View the status again to see what happened

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   README.md
```
</details>

And finally, commit (with a message)

```plaintext
git commit -m "initial commit"
```

<details><summary>Output</summary>

```plaintext
[main (root-commit) d8d9072] initial commit
 1 file changed, 7 insertions(+)
 create mode 100644 README.md
```

> Please note that hashes (the 7-digits hex code in the first line) will be different in your output.
</details>

Check the status again to see that there is "nothing to commit, working tree clean".

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
nothing to commit, working tree clean
```
</details>

## Make changes

Let's take some notes of what we did. Create a file `linear-workflow.md`

```plaintext
echo.> linear-workflow.md
```

Fill it with some content, e.g.

````plaintext
# Linear workflow

```
git init
```

* Create file README.md

```
git status
git add README.md
git status
git commit -m "initial commit"
git status
```
````

Save your changes.

Inspect what changed (you should always do that before staging and committing!)

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        linear-workflow.md

nothing added to commit but untracked files present (use "git add" to track)
```
</details>

Stage the new file

```plaintext
git add linear-workflow.md
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   linear-workflow.md
```
</details>

To "un-stage" files use `reset`

```plaintext
git reset linear-workflow.md
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        linear-workflow.md

nothing added to commit but untracked files present (use "git add" to track)
```
</details>

Stage the file again

```plaintext
git add linear-workflow.md
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   linear-workflow.md
```
</details>

Commit

```plaintext
git commit -m "added linear workflow"
```

<details><summary>Output</summary>

```plaintext
[main 43bc0a8] added linear workflow
 1 file changed, 15 insertions(+)
 create mode 100644 linear-workflow.md
```
</details>

## Inspect the history

To view the history of your project, use

```plaintext
git log
```

<details><summary>Output</summary>

```plaintext
commit 43bc0a86a7d49ad269f798360dfcea944713ef90 (HEAD -> main)
Author: ... <...@ufz.de>
Date:   Wed Jan 27 20:27:02 2021 +0100

    added linear workflow

commit d8d9072990da891df3624358b7a8fc473afb57f8
Author: ... <...@ufz.de>
Date:   Wed Jan 27 20:25:35 2021 +0100

    initial commit
```
</details>

We can also inspect the last commit with its message and diffs

```plaintext
git show
```

<details><summary>Output</summary>

```plaintext
commit 43bc0a86a7d49ad269f798360dfcea944713ef90 (HEAD -> main)
Author: ... <...@ufz.de>
Date:   Wed Jan 27 20:27:02 2021 +0100

    added linear workflow

diff --git a/linear-workflow.md b/linear-workflow.md
new file mode 100644
index 0000000..d039c21
--- /dev/null
+++ b/linear-workflow.md
@@ -0,0 +1,15 @@
+# Linear workflow
+
+```
+> git init
+```
+
+* Create file README.md
+
+```
+> git status
+> git add README.md
+> git status
+> git commit -m "initial commit"
+> git status
+```
\ No newline at end of file
```
</details>

> Depending on the size of your console window, the last line displayed may be `...`. In this case, scroll down with the arrow keys until the last line shows `(END)`. Then, press `Q` to finish.

> Please not the last line, `\ No newline at end of file`. It is a Unix convention that any text file should end with a newline character (`\n`), and as a programmer you should also follow this convention on other systems.  
> If you use the small `Copy to clipboard` icon in the top right corner of each code block instead of marking the text, it will automatically be copied with an extra newline at the end.

We can inspect an arbitraty commit by using its hash (at least the first 4 characters, typically 7). We view the first commit. Get the hash from the previous `log` output

```plaintext
git show d8d9072
```

We can also compare arbitrary commits

```plaintext
git diff <old-hash> <new-hash>
git diff d8d9072 43bc0a8
```

The output should look similar to that of the two calls above, just showing other diffs.

## Amend a commit

Oops, we forgot to link the file in `README.md`. This is only a small change that belongs to our last commit. We don't want to make a new commit, but we can amend the previous one.

Add the link to `README.md` and add an empty line at its end.

```plaintext
# Git Workshop

Content

* [Linear workflow](linear-workflow.md)
* ...
* ...
```

Also, fix the newline at the end of file `linear-workflow.md`

<details><summary>New file content</summary>

````plaintext
# Linear workflow

```
git init
```

* Create file README.md

```
git status
git add README.md
git status
git commit -m "initial commit"
git status
```
````
</details>

Check the status.

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md
        modified:   linear-workflow.md
```
</details>

With `git diff`, we can also view changes in files compared to what is committed (actually, committed + staged).

```plaintext
git diff
```

<details><summary>Output</summary>

```plaintext
diff --git a/README.md b/README.md
index e266d1c..6ecdc70 100644
--- a/README.md
+++ b/README.md
@@ -2,6 +2,6 @@

 Content

+* [Linear workflow](linear-workflow.md)
 * ...
 * ...
-* ...
\ No newline at end of file
diff --git a/linear-workflow.md b/linear-workflow.md
index d039c21..fda7fd8 100644
--- a/linear-workflow.md
+++ b/linear-workflow.md
@@ -12,4 +12,4 @@
 > git status
 > git commit -m "initial commit"
 > git status
-```
\ No newline at end of file
+```
```
</details>

```plaintext
git add README.md linear-workflow.md
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.md
        modified:   linear-workflow.md
```
</details>

To amend the last commit rather than create a new one, commit with option `--amend`

```plaintext
git commit --amend
```

As we did not enter a message this time, the text editor will pop up. Change the message if you want, save and close the editor.

<details><summary>Output</summary>

```plaintext
...
[main 1860049] added linear workflow
 Date: Wed Jan 27 20:27:02 2021 +0100
 2 files changed, 16 insertions(+), 1 deletion(-)
 create mode 100644 linear-workflow.md
```
</details>

Inspect the history again to see that there is no new commit

```plaintext
git log
```

<details><summary>Output</summary>

```plaintext
commit 1860049c5afd2ffed4661e8e36745aec3da57183 (HEAD -> main)
Author: ... <...@ufz.de>
Date:   Wed Jan 27 20:27:02 2021 +0100

    added linear workflow

commit d8d9072990da891df3624358b7a8fc473afb57f8
Author: ... <...@ufz.de>
Date:   Wed Jan 27 20:25:35 2021 +0100

    initial commit
```
</details>

However, the hash of the top-most commit changed. Further, the diff of the last commit now contains our change

```plaintext
git show
```

We can also show the diff for a single file

```plaintext
git show README.md
```

## Check out older states

Sometimes it may be necessary to revert a file to a state from the history, or just to view its old content.

```
git checkout 3cda487 -- README.md
```

(Use the hash of your first commit, it will be different!)

View the status to see what happened

```
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.md
```
</details>

We have staged changes in file `README.md`. It is a bit of a quirk that the file is staged, but that's how it is. View the file in the text editor to note that is now looks like after your first commit.

To return to the latest version, check out the file for `HEAD`

```
git checkout HEAD -- README.md
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
nothing to commit, working tree clean
```
</details>

[^1]: `echo.` writes text to the console. Using `>`, we redirect this (empty) text to the file and thereby create it. The dot `.` is required because without an argument (i.e. without text to write), `echo` switches the echo mode on or off, instead of writing text.

----

Continue with [Exercise 2](exercise-2.md)
