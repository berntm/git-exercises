# Exercise 2 - Non-linear workflow with branches

In this exercise, you will learn how to work with branches. You will create branches, merge them and resolve a conflict.

[[_TOC_]]

## Create a branch

We want to take some further notes from the workshop. However, we will now work on different branches and start with the chapter on how to inspect the history. First, create a new branch `inspect-history`

```plaintext
git branch inspect-history
```

Let's list the branches we have now

```plaintext
git branch
```

<details><summary>Output</summary>

```plaintext
  inspect-history
* main
```
</details>

Fine, but we are still on `main`. Checkout `inspect-history` and list branches again

```plaintext
git checkout inspect-history
git branch
```

<details><summary>Output</summary>

```plaintext
* inspect-history
  main
```
</details>

> The command `checkout` can do different things, depending on the context and the arguments.
> To reduce ambiguities, in newer versions of Git `switch` can be used instead of `checkout` to switch between branches,
> and `restore --staged` to un-stage changes.

We now have two branches and are currently on `inspect-history`. When using `git log`, you will see that both branches point to the same commit.

```plaintext
git log
```

<details><summary>Output</summary>

```plaintext
commit 1860049c5afd2ffed4661e8e36745aec3da57183 (HEAD -> inspect-history, main)
Author: ... <...@ufz.de>
Date:   Wed Jan 27 20:27:02 2021 +0100

    added linear workflow

commit d8d9072990da891df3624358b7a8fc473afb57f8
Author: ... <...@ufz.de>
Date:   Wed Jan 27 20:25:35 2021 +0100

    initial commit
```
</details>

BTW, `HEAD` shows where you currently are.

There is also a shortcut that creates a branch and checks it out immediately

```plaintext
git checkout -b inspect-history
```

## Make changes on a branch

Create a new file `inspecting.md`

```plaintext
echo.> inspecting.md
```

Fill it with some content, e.g.

````plaintext
# Inspecting

## Inspect the history

```
git log
```

## Inspecting commits

```
git show
git show 3cda487
git show README.md
```
````

Save your changes.

Link it in `README.md` and save again.

```plaintext
# Git Workshop

Content

* [Linear workflow](linear-workflow.md)
* [Inspecting](inspecting.md)
* ...
```

Now, commit as usual

```plaintext
git status
git diff
git add README.md inspecting.md
git status
git commit -m "added page on inspecting the history"
```

Let's view the history again

```plaintext
git log
```

For a more compact view, use

```plaintext
git log --oneline
```

<details><summary>Output</summary>

```plaintext
eba0f8f (HEAD -> inspect-history) added page on inspecting the history
1860049 (main) added linear workflow
d8d9072 initial commit
```
</details>

You will see that branch `inspect-history` is now one commit ahead of `main`. I.e. it moved along with our changes, while `main` did not.

## Switching between branches

With Git, it is possible to work on multiple branches in parallel.
Using `checkout`, we can simply switch between branches.
Observe the content of your project folder. There are now three Markdown files.

Checkout `main` and see the folder content again

```plaintext
git checkout main
```

Now, the files `inspecting.md` isn't there anymore.
When we checked out `main`, Git replaced the files in our working copy with the version referenced by `main`.
Check out branch `inspect-history` again to see that the working copy changes again and `inspecting.md` is back again.

```plaintext
git checkout inspect-history
```

## Another branch

We now make some parallel edits on an additional branch.
First, check out `main` again, as we want to start from there rather than from `inspect-history`

```plaintext
git checkout main
```

Create another branch `amending-commits` where we will take notes on that aspect

```plaintext
git checkout -b amending-commits
```

Create a new file `amending.md`

```plaintext
echo.> amending.md
```

Fill it the following content (and save...)

````plaintext
# Amending commits

```
git status
git diff
git add README.md
git commit --amend
```
````

Link the file in `README.md`

```plaintext
# Git Workshop

Content

* [Linear workflow](linear-workflow.md)
* [Amending commits](amending.md)
* ...
```

You may notice that the link to `inspecting.md` has gone. This is because we went back to branch `main`, which does not contain that change.

Commit as usual

```plaintext
git status
git diff
git add README.md amending.md
git status
git commit -m "added page on amending commits"
```

Let's inspect again what we have, with option `--oneline` for a more compact view

```plaintext
git log --oneline
```

<details><summary>Output</summary>

```plaintext
0639ed4 (HEAD -> amending-commits) added page on amending commits
1860049 (main) added linear workflow
d8d9072 initial commit
```
</details>

You may be surprised where branch `inspect-history` has gone. This is because by default, Git only shows what is part of the history of the current state, which `inspect-history` is not. To see the entire history as a (directed acyclic) graph, add options `--all` and `--graph`

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* 0639ed4 (HEAD -> amending-commits) added page on amending commits
| * eba0f8f (inspect-history) added page on inspecting the history
|/
* 1860049 (main) added linear workflow
* d8d9072 initial commit
```
</details>

You should see the entire graph now, and that we have diverged from `main` in two different directions.

## Merge

When the work on a branch is completed, it can be merged back into `main`. To merge, you need to be on the branch you want to merge the other branch into. So we check out `main`

```plaintext
git checkout main
```

Now, merge `amending-commits` into the current branch `main`

```plaintext
git merge amending-commits
```

<details><summary>Output</summary>

```plaintext
Updating 1860049..0639ed4
Fast-forward
 README.md   | 2 +-
 amending.md | 8 ++++++++
 2 files changed, 9 insertions(+), 1 deletion(-)
 create mode 100644 amending.md
```
</details>

Inspect the graph again

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* 0639ed4 (HEAD -> main, amending-commits) added page on amending commits
| * eba0f8f (inspect-history) added page on inspecting the history
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

Now, `main` and `amending-commits` point to the same commit, but somehow our branching topology has gone. This is because Git makes a so-called "fast-forward" merge in case there are no conflicts. We can prevent that with the option `--no-ff`. But first, we have to undo the merge.

## Reset a branch

To bring `main` back to the state before the merge, we make a hard reset to the previous commit (use the correct hash, the 3rd counted from the top!)

```plaintext
git reset --hard 1860049
```

<details><summary>Output</summary>

```plaintext
HEAD is now at 1860049 added linear workflow
```
</details>

Inspect again to see that we have what we had before the merge

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* 0639ed4 (HEAD -> amending-commits) added page on amending commits
| * eba0f8f (inspect-history) added page on inspecting the history
|/
* 1860049 (main) added linear workflow
* d8d9072 initial commit
```
</details>

## Merge with an extra commit

Now, we can merge without "fast-forward"

```plaintext
git merge amending-commits --no-ff
```

<details><summary>Output</summary>

```plaintext
Merge made by the 'recursive' strategy.
 README.md   | 2 +-
 amending.md | 8 ++++++++
 2 files changed, 9 insertions(+), 1 deletion(-)
 create mode 100644 amending.md
```
</details>

Inspect again to see that the branching topology is preserved now

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
*   e25d3cc (HEAD -> main) Merge branch 'amending-commits'
|\
| * 0639ed4 (amending-commits) added page on amending commits
|/
| * eba0f8f (inspect-history) added page on inspecting the history
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

Further, a new commit was created, with an automatic message (`Merge branch 'amending-commits'`)

## Merge with conflict

We can now try to also merge branch `inspect-history`

```plaintext
git merge inspect-history --no-ff
```

<details><summary>Output</summary>

```plaintext
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
Automatic merge failed; fix conflicts and then commit the result.
```
</details>

Git says that automatic merging failed due to a conflict in `README.md`, and that we should resolve the conflict and then commit. This is also a reminder to always read Git's output carefully.

To get some more insights, check the status

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Changes to be committed:
        new file:   inspecting.md

Unmerged paths:
  (use "git add <file>..." to mark resolution)
        both modified:   README.md
```
</details>

Again, read carefully! We have unmerged paths.
Further, is says we can resolve the conflicts and commit, or abort.
`merge` is an example of a modal command, i.e. we have to commit or abort the merge before we can continue to use Git normally.
For practice, we abort the merge and view the status:

```plaintext
git merge --abort
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
nothing to commit, working tree clean
```
</details>

That was for practice. Now let's merge again and resolve the conflict.

```plaintext
git merge inspect-history --no-ff
```

<details><summary>Output</summary>

```plaintext
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
Automatic merge failed; fix conflicts and then commit the result.
```
</details>

We get the same message as before: that automatic merging failed due to a conflict in `README.md`.

## Resolve conflicts

Open file `README.md`. It has the following content

```plaintext
# Git Workshop

Content

* [Linear workflow](linear-workflow.md)
<<<<<<< HEAD
* [Amending commits](amending.md)
=======
* [Inspecting](inspecting.md)
>>>>>>> inspect-history
* ...
```

The content between `<<<<<<< HEAD` and `>>>>>>> inspect-history` is where the conflict is, with the first part on `main` (our current `HEAD`), and the second part on `inspect-history`. Resolve the conflict and make the file look like this

```plaintext
# Git Workshop

Content

* [Linear workflow](linear-workflow.md)
* [Inspecting](inspecting.md)
* [Amending commits](amending.md)
```

Now that the conflict is resolved we can stage as usual

```plaintext
git status
git diff
git add README.md
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
All conflicts fixed but you are still merging.
  (use "git commit" to conclude merge)

Changes to be committed:
        modified:   README.md
        new file:   inspecting.md
```
</details>

Git tells us that all conflicts are fixed, but we are still merging.
We do what it suggests and commit

```plaintext
git commit
```

Leave the message in the popup as is and close it.

Through the commit, we finished the merge and are now no longer in "merge mode".
Confirm through checking the status

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
nothing to commit, working tree clean
```
</details>

Inspect the graph again

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
*   fc527c5 (HEAD -> main) Merge branch 'inspect-history'
|\
| * eba0f8f (inspect-history) added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 (amending-commits) added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

We see two nicely merged branches.

To finish our work, we should delete the merged branches. They are only pointers and not needed anymore. The topology will remain untouched by deleting them

```plaintext
git branch --delete amending-commits
git branch --delete inspect-history
```

And, a final check that everything worked as expected

```plaintext
git log --graph --oneline --all
```

```plaintext
*   fc527c5 (HEAD -> main) Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```

----

Continue with [Exercise 3](exercise-3.md)
