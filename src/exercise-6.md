# Exercise 6 - Tags for reproducibility

In this exercise, you will learn how use tags. You will create tags, push them to the remote repository and inspect them.

[[_TOC_]]

## Create tags

We want to add two tags, one to the commit before our merge request, and one to the current state.

View the graph to get the hash of the commit we want to tag first

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* 2b94010 (HEAD -> main, origin/main) added gitignore file to ignore documents
*   a3d243a Merge branch 'better-description' into 'main'
|\
| * 8226777 added note that the commands are git commands
| * 39d2543 enhanced description in README.md
|/
* f39b285 Update README.md
*   fc527c5 Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

Then, we add a tag `v0.1.0` to that commit

```plaintext
git tag v0.1.0 f39b285
```

Tagging the current commit is even simpler

```plaintext
git tag v0.1.1
```

View the graph again

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* 2b94010 (HEAD -> main, tag: v0.1.1, origin/main) added gitignore file to ignore documents
*   a3d243a Merge branch 'better-description' into 'main'
|\
| * 8226777 added note that the commands are git commands
| * 39d2543 enhanced description in README.md
|/
* f39b285 (tag: v0.1.0) Update README.md
*   fc527c5 Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

## Push tags

We now have two tags, but they are only local. Push all tags to the remote

```plaintext
git push --tags
```

<details><summary>Output</summary>

```plaintext
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
To https://git.ufz.de/mlange/git-workshop.git
 * [new tag]         v0.1.0 -> v0.1.0
 * [new tag]         v0.1.1 -> v0.1.1
```
</details>

## Inspect tags

To view a list of all tags, use

```
git tag
```

<details><summary>Output</summary>

```plaintext
v0.1.0
v0.1.1
```
</details>

Individual tags can be inspected with `git show`, just like commits

```plaintext
git show v0.1.0
git show v0.1.1
```

The output should look like when showing a commit.

----

Continue with [Exercise 7](exercise-7.md)
