# Exercise 7 - Keep branches up-to-date

In this exercise, you will learn how to keep branches up-to-date with changes made by collaborators (or yourself). You will back-merge branch `main` into a topic branch.

[[_TOC_]]

## Make changes on a branch

We continue our work on taking notes, again on a separate branch. Create and check out a new branch `branching` and check the result

```plaintext
git checkout -b branching
git branch
```

Create a new file `branching.md`

```plaintext
echo.> branching.md
```

Fill it with

````plaintext
# Branching

## Create a new branch

```
git branch inspect-history
git checkout inspect-history
git branch
```
````

Link the file in README.md

```plaintext
# Git Workshop

Notes taken in the OESA Git workshop in early 2021.

Use the project by browsing the files linked below, and by trying the Git commands listed there.

Content

* [Linear workflow](linear-workflow.md)
* [Inspecting](inspecting.md)
* [Amending commits](amending.md)
* [Branching](branching.md)
```

Then, commit as usual

```
git status
git diff
git add README.md branching.md
git status
git commit -m "added page on branching"
```

## Simulate changes by a collaborator

Meanwhile, a collaborator (or you) may have made changes to branch `main`, e.g. by merging a Merge Request. We simulate that by making an online edit.

Go to your project's page and edit `REAMDE.md`. Add a final note

```plaintext
# Git Workshop

Notes taken in the OESA Git workshop in early 2021.

Use the project by browsing the files linked below, and by trying the Git commands listed there.

Content

* [Linear workflow](linear-workflow.md)
* [Inspecting](inspecting.md)
* [Amending commits](amending.md)

For further information, see the [OESA Git Workshop book](https://oesa.pages.ufz.de/git-exercises).
```

Preview and commit your changes on target branch `main` (should be already set).

## Pull the changes

Pull the changes into your local repository

```plaintext
git checkout main
git pull
```

Then, check the graph

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* 653207e (HEAD -> main, origin/main) Update README.md
| * 3e445bb (branching) added page on branching
|/
* 2b94010 (tag: v0.1.1) added gitignore file to ignore documents
*   a3d243a Merge branch 'better-description' into 'main'
|\
| * 8226777 added note that the commands are git commands
| * 39d2543 enhanced description in README.md
|/
* f39b285 (tag: v0.1.0) Update README.md
*   fc527c5 Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

As expected, `main` has diverged.

## Back-merge into your branch

Before you continue your work on branch `branching`, you may want to update it to the latest state of `main`. Do that by merging `main` into `branching`

```plaintext
git checkout branching
git merge main
```

There is a conflict in `README.md` again, but you already know how to resolve it. Open the file locally and recolve the conflict. The result should look like this

```plaintext
# Git Workshop

Notes taken in the OESA Git workshop in early 2021.

Use the project by browsing the files linked below, and by trying the Git commands listed there.

Content

* [Linear workflow](linear-workflow.md)
* [Inspecting](inspecting.md)
* [Amending commits](amending.md)
* [Branching](branching.md)

For further information, see the [OESA Git Workshop book](https://oesa.pages.ufz.de/git-exercises).
```

Then, commit as usual

```plaintext
git status
git diff
git add README.md
git status
git commit
```

As before, close the editor popup to complete the commit.

Finally, inspect the graph again

```plaintext
git log --graph --oneline --all
```

```plaintext
*   e8ed3fd (HEAD -> branching) Merge branch 'main' into branching
|\
| * 653207e (origin/main, main) Update README.md
* | 3e445bb added page on branching
|/
* 2b94010 (tag: v0.1.1) added gitignore file to ignore documents
*   a3d243a Merge branch 'better-description' into 'main'
|\
| * 8226777 added note that the commands are git commands
| * 39d2543 enhanced description in README.md
|/
* f39b285 (tag: v0.1.0) Update README.md
*   fc527c5 Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```

This starts to look a bit confusing. Here is an alternative visualization that better conveys what really happened

```plaintext
 ╭>○    e8ed3fd (HEAD -> branching) Merge branch 'main' into branching
 ● │    653207e (main, origin/main) Update README.md
 │ ●    3e445bb added page on branching
 ├─╯
 ●      2b94010 [v0.1.1] added gitignore file to ignore documents
 ○<╮    a3d243a Merge branch 'better-description' into 'main'
 │ ●    8226777 added note that the commands are git commands
 │ ●    39d2543 enhanced description in README.md
 ├─╯
 ●      f39b285 [v0.1.0] Update README.md
 ○<──╮  fc527c5 Merge branch 'inspect-history'
 ○<╮ │  e25d3cc Merge branch 'amending-commits'
 │ ● │  0639ed4 added page on amending commits
 ├─╯ ●  eba0f8f added page on inspecting the history
 ├───╯
 ●      1860049 added linear workflow
 ●      d8d9072 initial commit
```

This visualization was created with [git-graph](https://github.com/mlange-42/git-graph). For an interactive version that also allows you to view commits, diffs, etc. see [git-igitt](https://github.com/mlange-42/git-igitt).
