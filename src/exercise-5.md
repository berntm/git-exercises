# Exercise 5 - Ignoring files

In this exercise, you will learn how to make Git ignore certain files. You will convert your Markdown files into a HTML file and write a `.gitignore` file to ignore it.

[[_TOC_]]

## Generate a HTML page

You may want to generate an all-in-one document of your notes to send it to a friend, or simply to see how it looks like in its rendered form. We will do that using [Pandoc](https://pandoc.org/).

First, create a sub-folder `documents`

```plaintext
mkdir documents
```

Next, we convert all Markdown files into a single HTML file

```plaintext
pandoc README.md linear-workflow.md inspecting.md amending.md -o documents/README.html
```

Navigate into folder `documents` using the explorer and view the output.

We could now commit the document. However, it is good practice to not commit anything that can be generated from the project's sources. This applies also to compiled binaries, like your model's `.exe` file.

But let's check the status first

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Your branch is up to date with 'origin/main'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        documents/

nothing added to commit but untracked files present (use "git add" to track)
```
</details>

which should show `documents/` as unstaged.

## Ignore files

One way to not commit a file it not to stage it. But there is a better way provided by Git: `.gitignore` files.

At the top level of your project (i.e. in `git-workshop`), create a file `.gitignore` (notice the dot! [^1]).

```plaintext
echo.> .gitignore
```

Open the file in your text editor. Here, we can list files, directories or file name patterns to be ignored, one entry per line. We will ignore the entire `documents` folder. Fill the file with the following content

```plaintext
/documents/
```

> For details on how to ignore directories, files, patterns etc. see the [gitignore documentation](https://git-scm.com/docs/gitignore).

Check the status again

```plaintext
git status
```

<details><summary>Output</summary>

```plaintext
On branch main
Your branch is up to date with 'origin/main'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        .gitignore

nothing added to commit but untracked files present (use "git add" to track)
```
</details>

The file `.gitignore` is listed as unstaged, but `documents/` disappeared, which is exactly what was our aim.

We can now commit as usual, and also push

```plaintext
git add .gitignore
git status
git commit -m "added gitignore file to ignore documents"
git push
```

and view the graph again

```plaintext
git log --graph --oneline
```

<details><summary>Output</summary>

```plaintext
* 2b94010 (HEAD -> main, origin/main) added gitignore file to ignore documents
*   a3d243a Merge branch 'better-description' into 'main'
|\
| * 8226777 added note that the commands are git commands
| * 39d2543 enhanced description in README.md
|/
* f39b285 Update README.md
*   fc527c5 Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

----

Continue with [Exercise 6](exercise-6.md)

[^1]: The leading dot is a naming convention for hidden files.
