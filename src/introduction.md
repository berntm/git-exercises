# Introduction

[[_TOC_]]

A collection of exercises for the OESA Git Workshop.

The workshop is intended to enable participants to work collaboratively using Git and GitLab on software and other projects.

No prior knowledge of Git is required. However, there are some prerequisites on software that needs to be installed to complete the exercises (see section [Requirements](#requirements)).

We will use the Git command line interface (CLI) because it exposes the terms and concepts directly. We know this may seem inconvenient, and a number of GUI frontends and IDE integrations exist. However, if you know how to use the CLI, you should be able to understand and use any GUI.

### How to use this book

In its current state, this book is not intended for self-study, as the context necessary to understand everything is provided in the workshop and is not laid out here. This may, however, change in a future version to make this book a stand-alone learning resource.

Each exercise proceeds on the state generated in the previous exercises. Thus, the intended way to use them is to work through them one by one. However, mind the limitations stated above.

The **slides** for the theory parts of the workshop are available as PDF [here](https://git.ufz.de/oesa/git-exercises/-/jobs/artifacts/master/raw/slides/slides.pdf?job=slides).

### Requirements

#### UFZ GitLab account

To work with UFZ GitLab, an account is required there. On the UFZ GitLab login page ([https://git.ufz.de/](https://git.ufz.de/)), use the `Contact us` link and write a short email to the WKDV. You should get an account quickly, usually at the same day.

Alternatively, a free GitLab account at [https://gitlab.com/](https://gitlab.com/) can be used.

#### Software

Essential software required to complete the exercises, and to work with Git in general, are:

* **Git**. Download the "official" Git client from [Git - Downloads](https://git-scm.com/downloads). Install with all settings as suggested by the installer!
* **A text editor**. Notepad (the Windows default) will do, but [Notepad++](https://notepad-plus-plus.org/) might be more comfortable to use

Further, we will use **Pandoc** in the exercises to convert Markdown files to HTML. It is not strictly required, but you may find it useful for your work also on other occasions.

On Windows, install Pandoc using the package installer available at [https://pandoc.org/installing.html](https://pandoc.org/installing.html). For other platforms, see the instructions there.

### Book sources

This book is generated from Markdown. Sources are available at UFZ GitLab:

[https://git.ufz.de/oesa/git-exercises](https://git.ufz.de/oesa/git-exercises)

### Cloning

To view the sources locally, clone them:

```plaintext
git clone https://git.ufz.de/oesa/git-exercises.git
```

### Contributing

If you have any suggestions how to improve this book, please [create an issue](https://git.ufz.de/oesa/git-exercises/-/issues) or make a merge request with your suggested changes.

----

Start with [Exercise 1](exercise-1.md)
